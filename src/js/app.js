angular.module('StappentellerPg', [
  'ngRoute',
  'mobile-angular-ui',
  'StappentellerPg.controllers.Main'
])

.config(function($routeProvider) {
  $routeProvider.when('/', {templateUrl:'home.html',  reloadOnSearch: false});
});